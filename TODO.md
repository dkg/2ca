2013-02-12 14:53:40-0500
------------------------

major changes that need thinking:

 * CRL distribution point?

 * OCSP?

 * OpenPGP OID

 * revocation
 
 * list of pending expirations
 
 * list of certified-but-expired keys
 
 * mechanism for marking certified-but-expired keys as ignorable
 
