2ca -- a Dual-Stack Certificate Authority
=========================================

The goal of 2ca is to allow the use of one set of secret key material
to produce OpenPGP and X.509 certifications on other keys.

The hope is also to encode best-practices so that standard operators
don't have to re-make the same decisions, while providing enough
flexibility that specific divergent needs can be met.

The basic work is a subcommand architecture.  It operates based on the
current working directory.

A sample setup to create a CA named "foo" would be:

    [ca]$ mkdir -p -m 0700 ~/private/foo
    [ca]$ cd ~/private/foo
    [ca]$ 2ca init "The Foo CA"

This will create a key, and produce two public certificates for the CA
-- X.509 certificate and an OpenPGP certificate.

When a new subscriber appears, they should make their own OpenPGP
certificate around the key they want to use, and send that to the CA
operator.

    [subscriber]$ export GNUPGHOME=/some/where/safe
    [subscriber]$ gpg --passphrase '' --quick-gen-key https://bar.example
    [subscriber]$ gpg --armor --export https://bar.example > bar.example.gpg
    [subscriber]$ echo here you go | mail -s 'cert for bar.example' -A bar.example.gpg ca-administrator@foo.example

The CA operator receives the certificate, and imports it like so:

    [ca]$ cd ~/private/foo
    [ca]$ 2ca import foo.example.gpg
    
After doing whatever steps their verification policy requires, if the
CA operator decides to certify the key, they identify it by OpenPGP
fingerprint:

    [ca]$ cd ~/private/foo
    [ca]$ 2ca certify tlsserver $FINGERPRINT

The CA operator will be prompted for one final review of the thing
being certified.

Then they can find the newly-issued X.509 certificate with:

    [ca]$ ls -l x509/tlsserver/$FINGERPRINT

and can send that file back to the subscriber as their new key.
